/**
 * Copyright 2020 cpke
 */

#ifndef XKIT_COMMON_CURL_HELPER_H
#define XKIT_COMMON_CURL_HELPER_H

#include <stdio.h>

// https://stackoverflow.com/questions/2329571/c-libcurl-get-output-into-a-string
typedef struct string {
    char *ptr;
    size_t len;
} string;

/**
 *
 * @param s
 */
void init_string(string *s);


size_t noop_cb(void *ptr, size_t size, size_t nmemb, void *data);

size_t writefunc(void *ptr, size_t size, size_t nmemb, string *s);

#endif //COMMON_CURL_HELPER_H
