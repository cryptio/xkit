/**
 * Copyright 2020 cpke
 */

#ifndef XKIT_COMMON_XBOX_H
#define XKIT_COMMON_XBOX_H

// XUIDs are represented as 64-bit integers which can hold up to 20 numbers
#define XUID_MAX 20
#define GAMERTAG_MAX 15
/* Xbox only specifies XSTS tokens as a string that has no defined maximum length, so we will use a buffer of 4096
 * to be safe
 */
#define XSTS_TOKEN_MAX 4096

#include <stdio.h>
#include "curl/curl.h"

/**
 * Get the name of the party that the XUID is currently in
 * @param xuid The XUID to check
 * @param buf A string buffer
 * @param size The buffer length
 * @param xsts_token The XSTS token we need for authentication
 * @return
 */
int get_party_name(const char *xuid, char *buf, size_t size, const char *xsts_token);

/**
 * Get the XUID of a gamertag
 * @param tag The gamertag to lookup
 * @param buf The buffer to write the XUID to
 */
int get_xuid(const char *tag, char *buf, size_t size, const char *xsts_token);

#endif //COMMON_XBOX_H
