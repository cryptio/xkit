/**
 * Copyright 2020 cpke
 */

#ifndef XKIT_COMMON_JSMN_HELPER_H
#define XKIT_COMMON_JSMN_HELPER_H

#define JSMN_HEADER
#include "jsmn/jsmn.h"

static int jsoneq(const char *json, jsmntok_t *tok, const char *s);

// Return next token, ignoring descendants
jsmntok_t *skip_token(jsmntok_t *token);

// Find the first member with the given name
jsmntok_t *object_get_member(const char *json, jsmntok_t *object, const char *name);

// Find the element at the given position of an array (starting at 0). */
jsmntok_t *array_get_at(jsmntok_t *object, int index);

#endif //COMMON_JSMN_HELPER_H
