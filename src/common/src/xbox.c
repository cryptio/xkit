/**
 * Copyright 2020 cpke
 */

#include "../include/xbox.h"
#include "../include/curl-helper.h"
#include "../include/jsmn-helper.h"
#include <string.h>


int get_party_name(const char *xuid, char *buf, size_t size, const char *xsts_token) {
    CURL *handle = curl_easy_init();
    CURLcode res;

    if (!handle) {
        fprintf(stderr, "Failed to acquire cURL handle\n");
        return -1;
    }

    string response;
    init_string(&response);

    // URL + XUID + params
    const size_t URL_MAX = 160;
    char url[160];
    strcpy(url,
           "https://sessiondirectory.xboxlive.com/serviceconfigs/7492baca-c1b4-440d-a391-b7ef364a8d40/sessiontemplates/chat/sessions?xuid=");
    strcat(url, xuid);
    strcat(url, "&followed=true");

    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);

    const size_t HEADER_SIZE = 9;

    char authorization_header[XSTS_TOKEN_MAX + 15];
    strcpy(authorization_header, "Authorization: ");
    strcat(authorization_header, xsts_token);

    const char *headers[] = {
            "x-xbl-contract-version: 107",
            "Cache-Control': 'no-store, must-revalidate, no-cache",
            "Accept: application/json",
            "PRAGMA: no-cache",
            "Accept-Language': 'en-AU, en, en-US, en",
            authorization_header,
            "Host: sessiondirectory.xboxlive.com",
            "Connection: Keep-Alive",
            "Accept-Encoding: identity"
    };

    struct curl_slist *chunk = NULL;

    for (size_t i = 0; i < HEADER_SIZE; ++i) {
        chunk = curl_slist_append(chunk, headers[i]);
    }

    // Set our custom set of headers
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);

    res = curl_easy_perform(handle);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return -1;
    }

    long http_code = 0;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &http_code);

    // Cleanup
    curl_easy_cleanup(handle);

    // Free custom headers
    curl_slist_free_all(chunk);

    jsmn_parser json_parser;
    jsmntok_t token_buffer[128]; // We expect no more than 128 JSON tokens

    jsmn_init(&json_parser);
    int r = jsmn_parse(&json_parser, response.ptr, strlen(response.ptr), token_buffer,
                       sizeof(token_buffer) / sizeof(token_buffer[0]));
    if (r < 0) {
        fprintf(stderr, "Failed to parse JSON: %d\n", r);
        return -1;
    }

    if (r < 1 || token_buffer[0].type != JSMN_OBJECT) {
        fprintf(stderr, "Object expected\n");
        return -1;
    }

    jsmntok_t *root = &token_buffer[0];

    jsmntok_t *results = object_get_member(response.ptr, root, "results");

    jsmntok_t *t = array_get_at(results, 0);

    if (!t)
        return -1;

    jsmntok_t *session_ref = object_get_member(response.ptr, t, "sessionRef");

    if (!session_ref)
        return -1;

    jsmntok_t *party_name = object_get_member(response.ptr, session_ref, "name");

    if (!party_name)
        return -1;

    const char *name = strndup(response.ptr + party_name->start, party_name->end - party_name->start);

    snprintf(buf, size, "%s", name);

    return 0;
}

int get_xuid(const char *tag, char *buf, size_t size, const char *xsts_token) {
    CURL *handle = curl_easy_init();
    CURLcode res;

    if (!handle) {
        fprintf(stderr, "Failed to acquire cURL handle\n");
        return -1;
    }

    string response;
    init_string(&response);

    const size_t URL_MAX = 237; // params + XUID_MAX + the url
    char url[URL_MAX];

    strcpy(url, "https://profile.xboxlive.com/users/gt(");
    strcat(url, curl_easy_escape(handle, tag, strlen(tag)));
    strcat(url, ")/profile/settings");
    strcat(url, curl_easy_escape(
            handle,
            "?settings=GameDisplayPicRaw,Gamerscore,Gamertag,AccountTier,XboxOneRep,PreferredColor,RealName,Bio,TenureLevel,Watermarks,Location,IsDeleted,ShowUserAsAvatar",
            157
    ));

    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);

    const size_t HEADER_SIZE = 8;

    char authorization_header[XSTS_TOKEN_MAX + 15];
    strcpy(authorization_header, "Authorization: ");
    strcat(authorization_header, xsts_token);

    const char *headers[] = {
            authorization_header,
            "Accept-Charset: UTF-8",
            "x-xbl-contract-version: 2",
            "Accept: application/json",
            "Content-Type: application/json",
            "Host: social.xboxlive.com",
            "Expect: 100-continue",
            "Connection: Keep-Alive"
    };

    struct curl_slist *chunk = NULL;

    for (size_t i = 0; i < HEADER_SIZE; ++i) {
        chunk = curl_slist_append(chunk, headers[i]);
    }

    // Set our custom set of headers
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);

    res = curl_easy_perform(handle);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return -1;
    }

    long http_code = 0;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &http_code);

    // Cleanup
    curl_easy_cleanup(handle);

    // Free custom headers
    curl_slist_free_all(chunk);

    jsmn_parser json_parser;
    jsmntok_t token_buffer[128]; // We expect no more than 128 JSON tokens

    jsmn_init(&json_parser);
    int r = jsmn_parse(&json_parser, response.ptr, strlen(response.ptr), token_buffer,
                       sizeof(token_buffer) / sizeof(token_buffer[0]));
    if (r < 0) {
        fprintf(stderr, "Failed to parse JSON: %d\n", r);
        return -1;
    }

    if (r < 1 || token_buffer[0].type != JSMN_OBJECT) {
        fprintf(stderr, "Object expected\n");
        return -1;
    }

    jsmntok_t *root = &token_buffer[0];

    jsmntok_t *profile_users = object_get_member(response.ptr, root, "profileUsers");

    jsmntok_t *t = array_get_at(profile_users, 0);

    jsmntok_t *id = object_get_member(response.ptr, t, "id");

    const char *xuid = strndup(response.ptr + id->start, id->end - id->start);

    snprintf(buf, size, "%s", xuid);

    return 0;
}