/**
 * Copyright 2020 cpke
 */

#include "../include/curl-helper.h"
#include <stdlib.h>
#include <string.h>

/**
 * Initialize a string struct
 * @param s The string to initialize
 */
void init_string(string *s) {
    s->len = 0;
    s->ptr = malloc(s->len + 1);
    if (s->ptr == NULL) {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    s->ptr[0] = '\0';
}

/**
 * Dummy cURL write function callback that simply returns the number of received bytes
 */
size_t noop_cb(void *ptr, size_t size, size_t nmemb, void *data) {
    return size * nmemb;
}

/**
 * cURL write function callback to store the response in a string
 * @param ptr
 * @param size
 * @param nmemb
 * @param s The string to write the data to
 * @return
 */
size_t writefunc(void *ptr, size_t size, size_t nmemb, string *s) {
    size_t new_len = s->len + size * nmemb;
    s->ptr = realloc(s->ptr, new_len + 1);
    if (s->ptr == NULL) {
        fprintf(stderr, "realloc() failed\n");
        exit(EXIT_FAILURE);
    }
    memcpy(s->ptr + s->len, ptr, size * nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size * nmemb;
}