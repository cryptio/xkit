# Copyright 2020 cpke

import base64
import binascii
import argparse

PARSER = argparse.ArgumentParser(description="Attempt to decode a secure device address retrieved from Xbox Live")
PARSER.add_argument("-d", help="The data to decode", required=True)
ARGS = PARSER.parse_args()


def decode_address(raw_bytes):
    """
    Attempt to decode a secure device address retrieved from Xbox Live

    :param raw_bytes:
    :return:
    """

    try:
        secure_device_address = base64.b64decode(raw_bytes)
    except binascii.Error:
        return "Unable to decode base64 encoded data"

    array_index = 0
    match = False

    if secure_device_address[0] == 0x01 and secure_device_address[1] == 0x00:
        last_four = [0, 0, 0, 0]

        for char in secure_device_address:
            array_index += 1
            last_four.pop(0)
            last_four.append(int(char))

            match_nums = [32, 1, 0, 0]
            if last_four == match_nums:
                match = True
                break

    if not match:
        return "(Hidden)"

    ipv4 = ""
    port = 0

    for x in range(4):
        octet = str(0xff - secure_device_address[(array_index + 8 + x)])

        ipv4 += octet if x == 0 else f'.{octet}'

    for x in range(2):
        port += (0xff - secure_device_address[(array_index + 7) - x]) * (1 + (0xff * x))


    return f'{ipv4}:{port}'


def main():
    print(decode_address(ARGS.d))


if __name__ == '__main__':
    main()

