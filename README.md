# xkit


A set of programs that leverage Xbox Live services to obtain user information, control parties, and more.

## Disclaimer
I do not condone the misuse of Xbox Live's REST API in order to ruin the gaming experience of other players. I also claim no credit for the discovery of how xkick, xpull, and xclose work. As indicated in the source code documentation, the original code was given to me by somebody else, who from there received it from another person. I simply rewrote it in C and improved it. 

## Credits
Credits go to Transmissional for making me aware of such a mechanism and its inner workings.

## Current status
- This issue was disclosed to Microsoft over half a year ago, without knowing exactly what has changed ~~I am hoping that none of it works anymore~~ it appears as if kicking users is still possible
- In May of 2020, xkick started having issues which pertained to the `If-Match` and `X-XBL-Correlation-Id` headers. The values had to be changed to restore functionality, which was done by sniffing Xbox traffic again with Fiddler. While it appears as if `If-Match` is the MD5 hash of something, while `X-XBL-Correlation-Id` is likely a GUID, the significance of the values are unknown. One can likely keep sniffing and replacing these headers as required, although it is not an ideal solution
- As of September 2020, Xbox has started using P2S for their parties. This likely broke some features of xkit, although it has not been checked

## How it works
The programs provided in this repository function by sending crafted headers to Xbox Live in order to perform requests that would otherwise not be accepted by Microsoft's services. These headers were found using Fiddler to act as an MITM for the HTTPS traffic, and observing the requests sent from different activities. 

## Project structure
`common`: Code shared between all programs in xkit

`xkick`: Allows you to remove users from parties even if you are not the leader

`xpull`: Obtains the public IP address and connection port of users in a party

`xclose`: Lets non-party leaders toggle whether or not the party is invite-only


## Building
In order to build this project, the following dependencies must be installed:
- libcurl
- cmake
- make

Next, navigate into one of the project directories (i.e xkick) and run the following:
```bash
cmake ./
make
```
This will generate a Makefile and then build the binary.

